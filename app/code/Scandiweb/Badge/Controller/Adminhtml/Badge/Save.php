<?php
/**
 * @module     Scandiweb/Badge
 * @author     Igors Trimailovs igors.trimailovs@scandiweb.com
 * @copyright  Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license    http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandiweb\Badge\Controller\Adminhtml\Badge;

use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * Save badge action.
 */
class Save extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session
     */
    const ADMIN_RESOURCE = 'Magento_Backend::content';

    /**
     * @var PostDataProcessor
     */
    protected $dataProcessor;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var \Scandiweb\Badge\Model\BadgeFactory
     */
    protected $badgeFactory;

    /**
     * @var \Scandiweb\Badge\Model\ImageUploader
     */
    protected $imageUploader;

    /**
     * @param Action\Context $context
     * @param DataPersistorInterface $dataPersistor
     * @param \Scandiweb\Badge\Model\BadgeFactory|null $badgeFactory
     * @param \Scandiweb\Badge\Model\ImageUploader $imageUploader
     */
    public function __construct(
        Action\Context $context,
        DataPersistorInterface $dataPersistor,
        \Scandiweb\Badge\Model\BadgeFactory $badgeFactory,
        \Scandiweb\Badge\Model\ImageUploader $imageUploader
    ) {
        $this->imageUploader = $imageUploader;
        $this->dataPersistor = $dataPersistor;
        $this->badgeFactory = $badgeFactory;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var array */
        $data = $this->getRequest()->getParams();

        if (isset($data['image'][0]['name']) && isset($data['image'][0]['tmp_name'])) {
            $data['image'] = $data['image'][0]['name'];
            $this->imageUploader->moveFileFromTmp($data['image']);
        } elseif (isset($data['image'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
            $data['image'] = $data['image'][0]['name'];
        } else {
            $data['image'] = '';
        }

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($data) {
            $model = $this->badgeFactory->create();
            $model->setData($data)->save($model);
            $this->messageManager->addSuccessMessage(__('You saved the badge.'));

            return $resultRedirect->setPath('*/*/');
        }
    }
}
