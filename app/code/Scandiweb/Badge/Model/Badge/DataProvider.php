<?php
/**
 * @module     Scandiweb/Badge
 * @author     Igors Trimailovs igors.trimailovs@scandiweb.com
 * @copyright  Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license    http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandiweb\Badge\Model\Badge;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\DataProvider\Modifier\PoolInterface;
use Scandiweb\Badge\Model\ResourceModel\Badge\CollectionFactory;

/**
 * Class DataProvider
 */
class DataProvider extends \Magento\Ui\DataProvider\ModifierPoolDataProvider
{
    /**
     * @var \Scandiweb\Badge\Model\ResourceModel\Badge\Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * Store manager
     *
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $blockCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     * @param PoolInterface|null $pool
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $blockCollectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = [],
        PoolInterface $pool = null,
        StoreManagerInterface $storeManager
    ) {
        $this->storeManager = $storeManager;
        $this->collection = $blockCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data, $pool);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();

        /** @var \Magento\Cms\Model\Block $block */
        foreach ($items as $badge) {
            $data = $badge->getData();

            if (isset($data['image'])) {
                $name = $data['image'];

                unset($data['image']);

                $data['image'][0] = [
                    'name' => $name,
                    'url' => $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'scandiweb/badge/' . $badge['image']
                ];
            }
            $this->loadedData[$badge->getId()] = $data;
        }

        $data = $this->dataPersistor->get('badge_badge');

        if (!empty($data)) {
            $badge = $this->collection->getNewEmptyItem();
            $badge->setData($data);
            $this->loadedData[$badge->getId()] = $badge->getData();
            $this->dataPersistor->clear('badge_badge');
        }

        return $this->loadedData;
    }
}
