<?php
/**
 * @module     Scandiweb/Badge
 * @author     Igors Trimailovs igors.trimailovs@scandiweb.com
 * @copyright  Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license    http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandiweb\Badge\Model\ResourceModel\Badge;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Badge Collection
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'badge_id';

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'badge_badge_collection';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'badge_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Scandiweb\Badge\Model\Badge::class, \Scandiweb\Badge\Model\ResourceModel\Badge::class);
    }
}
