<?php
/**
 * @module     Scandiweb/Badge
 * @author     Igors Trimailovs igors.trimailovs@scandiweb.com
 * @copyright  Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license    http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandiweb\Badge\Model\Attribute;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Scandiweb\Badge\Model\BadgeFactory;

/**
 * Class BadgsSelect
 */
class BadgeSelect extends AbstractSource
{

    /**
     * @var BadgeFactory
     */
    protected badgeFactory $badgeFactory;

    /**
     * @var array
     */
    protected $options;

    /**
     * Sliders constructor.
     *
     * @param BadgeFactory $badgeFactory
     */
    public function __construct(
        BadgeFactory $badgeFactory
    ) {
        $this->badgeFactory = $badgeFactory;
    }

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions(): array
    {
        return $this->toOptionArray();
    }

    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        /** var @array */
        $badges = $this->badgeFactory->create()->getCollection();
        $this->options = [[
            'value' => 0,
            'label' => __('No badge')
        ]];

        foreach ($badges as $badge) {
            if ($badge->getEnabled() == 1) {
                $this->options[] = [
                    'value' => $badge->getData()['badge_id'],
                    'label' => __($badge->getData()['title'])
                ];
            }
        }

        return $this->options;
    }
}
