define([
    'underscore',
    'Magento_Ui/js/grid/columns/select'
], function (_, Column) {
    'use strict';

    return Column.extend({
        defaults: {
            bodyTmpl: 'Scandiweb_Badge/ui/grid/cells/status'
        },
        getLabel: function (row) {
            if (row.enabled == '1') {
                return 'Enabled';
            }
            return 'Disabled';
        }
    });
});