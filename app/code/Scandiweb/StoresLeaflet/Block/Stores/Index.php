<?php
/**
 * @module     Scandiweb/Leaflet
 * @author     Igors Trimailovs igors.trimailovs@scandiweb.com
 * @copyright  Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license    http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandiweb\StoresLeaflet\Block\Stores;

class Index extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Scandiweb\StoreFinder\Model\Store
     */
    protected $store;

    /**
     * Display constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Scandiweb\StoreFinder\Model\Store $store
    ) {
        $this->store = $store;
        parent::__construct($context);
    }

    /**
     * @return array of stores
     */
    public function getStores()
    {
        $collection = $this->store->getCollection();

        return $collection;
    }
}
