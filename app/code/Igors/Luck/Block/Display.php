<?php

/**
 * @module Igors/Luck
 * @author Igors Trimailovs igors.trimailovs@scandiweb.com
 * @copyright Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Igors\Luck\Block;

class Display extends \Magento\Framework\View\Element\Template
{

    /**
     * Display constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     */
    public function __construct(\Magento\Framework\View\Element\Template\Context $context)
    {
        parent::__construct($context);
    }
}
