<?php

/**
  * @module     Igors/Stores
  * @author     Igors Trimailovs
  * @copyright  Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
  * @license    http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
  */

namespace Igors\Stores\Setup;

use Igors\Stores\Setup\Migration\Configuration;
use Igors\Stores\Setup\Migration\CreateStore;
use Magento\Framework\Setup\UpgradeDataInterface;
use Scandiweb\Migration\Setup\AbstractUpgradeData;

class UpgradeData extends AbstractUpgradeData
{
    /**
     * @var string[]
     */
    protected $migrations = [
        '1.0.1' => CreateStore::class,
        '1.0.1' => Configuration::class
    ];
}
