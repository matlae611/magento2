<?php

/**
 * @module     Igors/Stores
 * @author     Igors Trimailovs
 * @copyright  Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license    http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Igors\Stores\Setup\Migration;

use Magento\Directory\Model\Currency;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\Setup\SetupInterface;
use Magento\Store\Model\ResourceModel\Store;
use Magento\Store\Model\StoreFactory;
use Magento\Theme\Model\ThemeFactory;
use Scandiweb\Migration\Api\MigrationInterface;

class Configuration implements MigrationInterface
{

    /**
     * @var ConfigInterface
     */
    protected $configInterface;

    /**
     * @var StoreFactory
     */
    protected $storeFactory;

    /**
     * @var Currency
     */
    protected $currency;

    /**
     * @var ThemeFactory
     */
    protected $themeFactory;

    /*
     * Config path consts
     */
    const CONFIG_THEME_PATH = 'design/theme/theme_id';
    const CONFIG_CURRENCY_DEFAULT_PATH = 'currency/options/default';
    const CONFIG_CURRENCY_ALLOW_PATH = 'currency/options/allow';
    const CONFIG_PRODUCT_URL_SUFFIX_PATH = 'catalog/seo/product_url_suffix';
    const CONFIG_CATEGORY_URL_SUFFIX_PATH = 'catalog/seo/category_url_suffix';
    const CONFIG_PRODUCT_URL_SUFFIX_DEFAULT_PATH = 'catalog/seo/product_url_suffix';
    const CONFIG_CATEGORY_URL_SUFFIX_DEFAULT_PATH = 'catalog/seo/category_url_suffix';

    /**
     * Configuration constructor.
     * @param ConfigInterface $configInterface
     * @param StoreFactory $storeFactory
     * @param ThemeFactory $themeFactory
     * @param Currency $currency
     */
    public function __construct(
        ConfigInterface $configInterface,
        StoreFactory $storeFactory,
        ThemeFactory $themeFactory,
        Currency $currency
    ) {
        $this->themeFactory = $themeFactory;
        $this->storeFactory = $storeFactory;
        $this->configInterface = $configInterface;
        $this->currency = $currency;
    }

    /**
     * @param SetupInterface|null $setup
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function apply(SetupInterface $setup = null)
    {
        //Changing name of default store
        $store = $this->storeFactory->create();
        $store->load('1', 'store_id');
        $store->setName('Germany EUR Store')->save();

        //Applying skandi/default theme to german store
        $theme = $this->themeFactory->create();
        $theme->load('skandi/german', 'theme_title');
        $storeEUR = $this->storeFactory->create();
        $storeEUR->load('Germany EUR Store', 'name');
        $storeEurID = $storeEUR->getId();
        $this->configInterface->saveConfig(self::CONFIG_THEME_PATH, $theme->getId(), 'stores', $storeEurID);

        //Applying skandi/default theme to german store
        $theme = $this->themeFactory->create();
        $theme->load('skandi/default', 'theme_title');
        $storeGBP = $this->storeFactory->create();
        $storeGBP->load('English GBP Store', 'name');
        $storeUkID = $storeGBP->getId();
        $this->configInterface->saveConfig(self::CONFIG_THEME_PATH, $theme->getId(), 'stores', $storeUkID);

        //Setting currency to stores
        $this->configInterface->saveConfig(self::CONFIG_CURRENCY_DEFAULT_PATH, 'EUR', 'stores', $storeEurID);
        $this->configInterface->saveConfig(self::CONFIG_CURRENCY_DEFAULT_PATH, 'GBP', 'stores', $storeUkID);
        $this->configInterface->saveConfig(self::CONFIG_CURRENCY_ALLOW_PATH, 'GBP', 'stores', $storeUkID);
        $this->configInterface->saveConfig(self::CONFIG_CURRENCY_ALLOW_PATH, 'EUR', 'stores', $storeEurID);

        //Removing .html suffix from pages
        $this->configInterface->saveConfig(self::CONFIG_PRODUCT_URL_SUFFIX_PATH, null, 'stores', $storeUkID);
        $this->configInterface->saveConfig(self::CONFIG_CATEGORY_URL_SUFFIX_PATH, null, 'stores', $storeUkID);
        $this->configInterface->saveConfig(self::CONFIG_PRODUCT_URL_SUFFIX_PATH, null, 'stores', $storeEurID);
        $this->configInterface->saveConfig(self::CONFIG_CATEGORY_URL_SUFFIX_PATH, null, 'stores', $storeEurID);
        $this->configInterface->saveConfig(self::CONFIG_PRODUCT_URL_SUFFIX_DEFAULT_PATH, null, 'default', 0);
        $this->configInterface->saveConfig(self::CONFIG_CATEGORY_URL_SUFFIX_DEFAULT_PATH, null, 'default', 0);

        //Setting currency rates
        $rates = [
            ['USD' => ['GBP' => '0.77']],
            ['USD' => ['EUR' => '0.85']]
        ];
        $this->currency->saveRates($rates);
    }
}
