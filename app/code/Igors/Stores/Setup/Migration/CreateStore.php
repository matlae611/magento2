<?php

/**
  * @module     Igors/Stores
  * @author     Igors Trimailovs
  * @copyright  Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
  * @license    http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
  */

namespace Igors\Stores\Setup\Migration;

use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Setup\SetupInterface;
use Magento\Store\Model\GroupFactory;
use Magento\Store\Model\ResourceModel\Group;
use Magento\Store\Model\ResourceModel\Store;
use Magento\Store\Model\ResourceModel\Website;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\WebsiteFactory;
use Scandiweb\Migration\Api\MigrationInterface;

class CreateStore implements MigrationInterface
{

    /**
     * @var WebsiteFactory
     */
    protected $websiteFactory;

    /**
     * @var Website
     */
    protected $websiteResourceModel;

    /**
     * @var StoreFactory
     */
    protected $storeFactory;

    /**
     * @var GroupFactory
     */
    protected $groupFactory;

    /**
     * @var Group
     */
    protected $groupResourceModel;

    /**
     * @var Store
     */
    protected $storeResourceModel;

    /**
     * CreateStore constructor.
     * @param WebsiteFactory $websiteFactory
     * @param Store $storeResourceModel
     * @param StoreFactory $storeFactory
     * @param GroupFactory $groupFactory
     */
    public function __construct(
        WebsiteFactory $websiteFactory,
        Store $storeResourceModel,
        StoreFactory $storeFactory,
        GroupFactory $groupFactory
    ) {
        $this->websiteFactory = $websiteFactory;
        $this->storeFactory = $storeFactory;
        $this->groupFactory = $groupFactory;
        $this->storeResourceModel = $storeResourceModel;
    }

    /**
     * @param SetupInterface|null $setup
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function apply(SetupInterface $setup = null)
    {
        $store = $this->storeFactory->create();
        $store->load('english_gbp_store');

        if (!$store->getId()) {
            $group = $this->groupFactory->create();
            $group->load('Main Website Store', 'name');
            $store->setCode('english_gbp_store');
            $store->setName('English GBP Store');
            $website = $this->websiteFactory->create();
            $website->load(1);
            $store->setWebsite($website);
            $store->setGroupId($group->getId());
            $store->setData('is_active', '1');
            $this->storeResourceModel->save($store);
        }
    }
}
