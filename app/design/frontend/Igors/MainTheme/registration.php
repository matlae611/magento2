<?php
/**
 * @theme Igors/Theme
 * @author Igors Trimailovs igors.trimailovs@scandiweb.com
 * @copyright Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::THEME,
    'frontend/Igors/MainTheme',
    __DIR__
);
