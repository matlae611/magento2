var config = {
    map: {
        '*': {
            'connectSlick' : 'js/connectSlick',
            'slick': 'js/slick',
            'stickyHeader': 'js/stickyHeader'
        }
    },
};
