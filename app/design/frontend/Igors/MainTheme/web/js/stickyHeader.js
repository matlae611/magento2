require([ 'jquery' ], function ($) {

    function hide()
    {
        $('.header.content').addClass('max-height-0');
        $('.header.content').addClass('p-0');
        $('.logo').addClass('m-0');
    }

    function show()
    {
        $('.header.content').removeClass('max-height-0');
        $('.header.content').removeClass('p-0');
        $('.logo').removeClass('m-0');
    }

    $(window).scroll(function () {

        //Making header sticky
        if ($(window).scrollTop() >= $('.page-header').offset().top && !($('.page-header').hasClass('custom-sticky-header'))) {
            $('.page-header').addClass('custom-sticky-header');
            $('#maincontent').css('top', $('.page-header').height());
        } else if ($(window).scrollTop() == 0) {
            $('.page-header').removeClass('custom-sticky-header');
            $('#maincontent').css('top', '0');
        }

        //Collapsing/expanding header
        if ($(document).scrollTop() >= 400 ) {
            hide();
        } else {
            show();
        }
    });
});