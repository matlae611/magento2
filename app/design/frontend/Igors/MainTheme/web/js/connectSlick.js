require([
    'jquery',
    'slick'
], function ($) {
    console.log('123');
    $(document).ready(function () {
        $(".widget-product-grid").slick({
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            arrows: true,
            nextArrow: '<span class="chevron arrow-right c-right"></span>',
            prevArrow: '<span class="chevron arrow-left c-left "></span>',
            responsive: [
                {
                    breakpoint: 1279,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 1023,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    });
});