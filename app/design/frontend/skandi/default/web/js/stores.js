require([ 'jquery' ], function ($) {

    let map = L.map('map').setView([51.505, -0.09], 13);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoiaWdvcnNtYXRsYWUiLCJhIjoiY2tmczYzbmNmMDdoMzJxbWpvNDh1anZ1OSJ9.5EtiifmjmwfzI4L47Ptlfg'
    }).addTo(map);

    let markers = [];
    if (stores !== undefined) {
        stores.forEach(element => {
            markers.push(L.marker([element.latitude, element.longitude]).addTo(map));
            markers[markers.length-1].bindPopup(`<b>${element.name}</b>${element.hours === null ? '' : element.hours}`);
        });

        let group = new L.featureGroup(markers);
        map.fitBounds(group.getBounds());
    }

    //Switching map view on click on store in store list
    $('.shopitem').click(function () {
        let store = stores.find(element => element.id == $(this).data('id'));
        map.setView([store['latitude'], store['longitude']], 13);
        markers[stores.findIndex(element => element.id == $(this).data('id'))].openPopup();
    });

    //input autocomplete
    if (stores !== undefined) {
        let autocompleteData = stores.map(element => { return element['name'] });
        var options = {
            data: autocompleteData
        };
        $('#searchinput').easyAutocomplete(options);
    }

    //Search action
    $('.searchbtn').click(function () {
        let store = stores.find(element => element.name == $('#searchinput').val());
        map.setView([store.latitude, store.longitude], 13);
        markers[stores.findIndex(element => element.name == $('#searchinput').val())].openPopup();
    });
});
