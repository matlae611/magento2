var config = {
    map: {
        '*': {
            'leaflet' : 'js/leaflet',
            'autocomplete' : 'js/easyautocomplete',
            'stores' : 'js/stores'
        }
    },
};
