'use-strict';

const gulp = require('gulp'),
  context = require('../context'),
  linter = require('gulp-scss-lint'),
  fs = require('fs'),
  scssSources = require('./helper/build-scss-sources'),
  console = require('better-console');

module.exports = () => {
  let linterOptions = context.config.scss.linting;

  if (!fs.existsSync(linterOptions.config)) {
    console.error('.scss.lint.yaml is missing in your root directory');
    process.exit(1);
  }

  scssSources(context).forEach(config => {
      gulp.src(config.source).pipe(linter(linterOptions)).pipe(linter.failReporter());
  });
};