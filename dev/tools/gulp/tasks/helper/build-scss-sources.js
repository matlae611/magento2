const _ = require('lodash'),
  path = require('path');

/**
 * @param magentoRoot
 * @param theme
 *
 * @returns {Array}
 */
let resolvePaths = (magentoRoot, theme) => {
  let resolvedPaths = [];
  let pathsToResolve = theme.scss.options.includePaths;

  _.forEach(pathsToResolve, (pathToResolve) => {
    resolvedPaths.push(path.resolve(magentoRoot, 'node_modules', pathToResolve));
  });

  return resolvedPaths;
};

module.exports = (context) => {
  let themeRegister = [],
    options = {},
    syncOptions = {};

  _.forEach(context.themes, (theme) => {
    let sourcePath = path.resolve(context.magentoRoot, 'app/design', theme.area, theme.name)
      + (theme.source ? theme.source : context.config.scss.source + context.config.scss.mask);

    let destinationPath = path.resolve(context.magentoRoot, 'app/design', theme.area, theme.name)
      + (theme.destination ? theme.destination : context.config.scss.destination);

    /**
     * Take scss.options.includePath from theme.js
     *
     * @type {Object}
     */
    theme.scss.options.includePaths = resolvePaths(context.magentoRoot, theme);
    _.assignIn(options, context.config.scss.options, theme.scss.options);
    _.assignIn(syncOptions, context.config.browsersync, theme.browsersync);
    let builtTheme = {
      source: sourcePath,
      destination: destinationPath,
      autoprefixer: context.config.autoprefixer,
      options: options,
      browsersync: require('browser-sync').create(),
      syncOptions: syncOptions,
    };
    
    themeRegister.push(builtTheme);
  });
  
  return themeRegister;
};
