'use-strict';

const autoprefixer = require('gulp-autoprefixer'),
  csso = require('gulp-csso'),
  gulp = require('gulp'),
  gulpif = require('gulp-if'),
  linter = require('gulp-scss-lint'),
  scss = require('gulp-sass'),
  fs = require('fs'),
  console = require('better-console'),
  sourcemaps = require('gulp-sourcemaps');

module.exports = (context, config, isWatch = false) => {
  let linterOptions = context.config.scss.linting,
    isDevMode = process.env.NODE_ENV === 'development',
    isProdMod = process.env.NODE_ENV === 'production';
  
  if (!fs.existsSync(linterOptions.config)) {
    console.error('.scss.lint.yaml is missing in your root directory');
    process.exit(1);
  }

  return gulp.src(config.source)
    .pipe(gulpif(isDevMode, linter(linterOptions)))
    .pipe(gulpif(isDevMode && !isWatch, linter.failReporter('E')))
    .pipe(gulpif(isDevMode, sourcemaps.init()))
    .pipe(scss(config.options))
    .on('error', scss.logError)
    .pipe(gulpif(isDevMode, sourcemaps.write()))
    .pipe(gulpif(isProdMod, autoprefixer(config.autoprefixer)))
    .pipe(gulpif(isProdMod, csso({restructure: false})))
    .pipe(gulp.dest(config.destination))
    .pipe(gulpif(isDevMode, config.browsersync.stream()));
};
